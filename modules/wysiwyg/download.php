
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<meta name="description" content="TinyMCE is a platform independent web based Javascript HTML WYSIWYG editor control released as Open Source under LGPL by Moxiecode Systems AB. It has the ability to convert HTML TEXTAREA fields or other HTML elements to editor instances. TinyMCE is very easy to integrate into other CMS systems." />
<meta name="keywords" content="tinymce,wysiwyg,javascript,open source,LGPL,dhtml,editor,control,cms,online,html,clientside,xhtml,mozilla,firefox,safari,explorer,midas,execcommand,contenteditable,filemanager,imagemanager,plugins" />
<meta name="copyright" content="Moxiecode Systems AB" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="/css/compress.php" media="screen" />
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie_fixes.css" media="screen" /><![endif]-->
<script type="text/javascript" src="/js/general.js"></script>
<link rel="alternate" type="application/rss+xml" title="TinyMCE News RSS" href="http://tinymce.moxiecode.com/forum_news_rss.php" />
<title>TinyMCE - Download</title>
</head>
<body>

<div id="header">

	<div class="wrapper">

		<div class="content">

			<div class="left" id="logo">
				<a href="/index.php"></a>
			</div>

			<div class="right tright">

				<div class="tleft">

					<div id="tsearch">
						<div class="form">
							<form id="smallsearch" method="get" action="/search.php">
								<div><input type="text" name="searchquery" id="s" value="Search" size="18" onfocus="this.value='';" /></div>
								<div><a href="javascript:document.forms['smallsearch'].submit();"><!-- IE --></a></div>
							</form>
						</div>
					</div>

					<!-- document write this so it doesn't get indexed -->
<script type="text/javascript">// <![CDATA[
document.write('<div id=\"tquote\">' +
'<div class=\"text\">' +
'\"I am absolutely stunned by this innovation. Such power is only matched by its simplicity in implementation. Fantastic work guys!\"' +
'<em>- Stephen Scott, <a href=\"http://www.sassquad.com\" target=\"_blank\">Sassquad.com<'+'/a><'+'/em>' +
'<'+'/div>' +
'<'+'/div>');
// ]]>
</script>
				</div>
			
			</div>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

</div>


<div id="navigation" class="nosub">
	<div class="wrapper">

		<div class="links">
		
			<ul class="tabs">
<li><span><a href="/index.php">Home</a></span></li><li><span><a href="/using.php">About</a></span></li><li class="selected_tab"><span><a href="/download.php">Download</a></span></li><li><span><a href="/examples/full.php">Examples</a></span></li><li><span><a href="/documentation.php">Documentation</a></span></li><li><span><a href="/plugins.php">Plugins</a></span></li><li><span><a href="/support.php">Support</a></span></li><li><span><a href="/punbb">Forum</a></span></li><li><span><a href="/guestbook.php">Guestbook</a></span></li>			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>
</div>



<div id="main">
	<div class="wrapper">

		<div id="main_content">
			

			<div class="left" id="main_left">
				<div class="p15">

					<h1>Download</h1>

					<table class="datatable">
						<tr>
							<th colspan="2">Packages</th>
						</tr>

						<tr class="alt">
							<td class="w150">Main package</td>
							<td>
								<p>Contains all you need for production usage.</p>
								<a href="https://sourceforge.net/projects/tinymce/files/TinyMCE/3.2.7/tinymce_3_2_7.zip/download">tinymce_3_2_7.zip</a>
							</td>
						</tr>

						<tr>
							<td>Development package</td>
							<td>
								<p>This package contains development tools and full source code.</p>
								<a href="https://sourceforge.net/projects/tinymce/files/TinyMCE/3.2.7/tinymce_3_2_7_dev.zip/download">tinymce_3_2_7_dev.zip</a>
							</td>
						</tr>

						<tr>
							<td>jQuery package</td>
							<td>
								<p>This package contains special jQuery build of TinyMCE and a jQuery integration plugin.</p>
								<a href="https://sourceforge.net/projects/tinymce/files/TinyMCE/3.2.7/tinymce_3_2_7_jquery.zip/download">tinymce_3_2_7_jquery.zip</a>
							</td>
						</tr>

						<tr class="alt">
							<td>Language packs</td>
							<td>
								<p>Languages are available though our translation service.</p>
								<a href="download_i18n.php">Language pack service &gt;&gt;</a>
							</td>
						</tr>
					</table>

					<table class="datatable">
						<tr>
							<th>Compressor type</th>
							<th>Filename</th>
						</tr>
						<tr class="alt">
							<td>Compressor PHP</td>
							<td><a href="http://prdownloads.sourceforge.net/tinymce/tinymce_compressor_php_2_0_2.zip?download">tinymce_compressor_php_2_0_2.zip</a></td>
						</tr>
						<tr>
							<td>Compressor .NET</td>
							<td><a href="http://prdownloads.sourceforge.net/tinymce/tinymce_compressor_net_2_0_2.zip?download">tinymce_compressor_net_2_0_2.zip</a></td>
						</tr>
						<tr class="alt">
							<td>Compressor JSP</td>
							<td><a href="http://prdownloads.sourceforge.net/tinymce/tinymce_compressor_jsp_2_0_2.zip?download">tinymce_compressor_jsp_2_0_2.zip</a></td>
						</tr>
						<tr>
							<td>Compressor Coldfusion</td>
							<td><a href="http://prdownloads.sourceforge.net/tinymce/tinymce_compressor_cfm_2_1_0.zip?download">tinymce_compressor_cfm_2_1_0.zip</a></td>
						</tr>
					</table>

					<table class="datatable">
						<tr>
							<th colspan="2">Other compontents</th>
						</tr>

						<tr class="alt">
							<td>.NET package</td>
							<td>
								<p>This package contains development tools and full source code.</p>
								<a href="http://prdownloads.sourceforge.net/tinymce/tinymce_package_dotnet_1_0a2.zip?download">tinymce_package_dotnet_1_0a2.zip</a>
							</td>
						</tr>

						<tr>
							<td class="w150">PHP Spellchecker</td>
							<td>
								<p>Spellchecker plugin for TinyMCE with a PHP backend.</p>
								<a href="http://prdownloads.sourceforge.net/tinymce/tinymce_spellchecker_php_2_0_2.zip?download">tinymce_spellchecker_php_2_0_2.zip</a>
							</td>
						</tr>
					</table> 

					<p>If you are looking for older releases check the <a href="http://sourceforge.net/project/showfiles.php?group_id=103281">sourceforge downloads page</a>.</p>
				</div>
			</div>

			<div class="right" id="main_right">
							<div class="push" id="push_download">
					<div class="push_title">Download</div>
					<div class="push_content">
						<div class="left mr10">
							<a href="/download.php" class="download_tinymce"></a>
							<div class="version mb10">Version: 3.2.7</div>
							<form id="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">
								<div>
								<input type="hidden" name="cmd" value="_xclick" />
								<input type="hidden" name="business" value="spocke@moxiecode.com" />
								<input type="hidden" name="item_name" value="TinyMCE Donation" />
								<input type="hidden" name="currency_code" value="EUR" />
								</div>
							</form>
						</div>
						<ul class="more left">
							<li><span class="info"><a href="http://tinymce.moxiecode.com/punbb/viewtopic.php?id=18154">Release notes</a></span></li>
							<li><span class="changelog"><a href="changelog.php">Changelog</a></span></li>
							<li><span class="license"><a href="license.php">License</a></span></li>
							<li><span class="sf"><a href="http://www.sourceforge.net/projects/tinymce">Sourceforge project</a></span></li>
							<li><span class="donate"><a href="#" onclick="document.forms['_xclick'].submit()">Donate</a></span></li>
						</ul>
						<div class="clearer">&nbsp;</div>
					</div>
				</div>
								<div class="push" id="push_plugins">
					<div class="push_title">Plugins</div>
					<div class="push_content">
						<div class="plugin" id="plugin_mcfilemanager">
							<div class="title"><h3><a href="plugins_filemanager.php">MCFileManager</a></h3></div>
							<div class="icon"><a href="plugins_filemanager.php"></a></div>
							<div class="content">
								<div class="description">
									Need a <span class="highlight">File Manager</span> for your TinyMCE implementation? Then check this out, integrate it into TinyMCE or use it with <span class="highlight">any editor</span> out there. Available for .NET and PHP.
								</div>
							</div>
							<div class="clearer">&nbsp;</div>

							<p>&#187; <a href="plugins_filemanager.php" title="More information">More information / Buy</a> (Demo available)</p>
						</div>

						<div class="plugin" id="plugin_mcimagemanager">
							<div class="title"><h3><a href="plugins_imagemanager.php">MCImageManager</a></h3></div>
							<div class="icon"><a href="plugins_imagemanager.php"></a></div>
							<div class="content">
								<div class="description">
									Need an <span class="highlight">Image Manager</span> for your TinyMCE implementation? Then check this out, integrate it into TinyMCE or use it with <span class="highlight">any editor</span> out there. Available for .NET and PHP.
								</div>
							</div>
							<div class="clearer">&nbsp;</div>

							<p>&#187; <a href="plugins_imagemanager.php" title="More information">More information / Buy</a> (Demo available)</p>
						</div>

						Need one for <a href="plugins_other_platforms.php">ASP</a> or <a href="plugins_other_platforms.php">JSP</a>?
					</div>
				</div>	
			</div>

			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>

<div id="footer">
	<div class="wrapper">
		<div class="content">

			<p class="right">
<a href="/index.php">Home</a> <span class="separator">|</span> <a href="/using.php">About</a> <span class="separator">|</span> <a href="/download.php">Download</a> <span class="separator">|</span> <a href="/examples/full.php">Examples</a> <span class="separator">|</span> <a href="/documentation.php">Documentation</a> <span class="separator">|</span> <a href="/plugins.php">Plugins</a> <span class="separator">|</span> <a href="/support.php">Support</a> <span class="separator">|</span> <a href="/punbb">Forum</a> <span class="separator">|</span> <a href="/guestbook.php">Guestbook</a>			</p>

			<p class="left">
				<a href="/index.php">TinyMCE</a> - A Free Javascript WYSIWYG Editor
			</p>

			<div class="clearer">&nbsp;</div>

			<p class="small">
				<img src="/img/logo_moxiecode.gif" alt="" class="left mr15" />
				&#169; 2003-2009 <a href="http://www.moxiecode.com">Moxiecode Systems</a> AB. All rights Reserved.<br/>
				Provided for developers anywhere, everywhere, anytime and with any content.
			</p>

			<div class="clearer">&nbsp;</div>

		</div>
	</div>
</div>

</body>
</html>
