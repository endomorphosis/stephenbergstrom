# $Id: parcel.pot,v 1.1.2.1 2007/05/03 14:52:05 darrenoh Exp $
#
# LANGUAGE translation of Drupal (parcel.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  parcel.module,v 1.27.2.3.2.2.2.3 2007/03/07 23:00:48 gordon
#  parcel.install,v 1.3.6.1 2007/02/14 06:48:25 sime
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 10:39-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: parcel.module:14
msgid "<p>This module provides a way for you to create a package of products. First, individually create the products and then create a product package and add those items. These actions can be done using any of the options under <a href=\"!node_add\">create content</a>.</p>"
msgstr ""

#: parcel.module:16
msgid "A collection of products is a package or group of items sold as a whole."
msgstr ""

#: parcel.module:57
msgid "You must add at least one existing product to this group of products to sell."
msgstr ""

#: parcel.module:60
msgid "Please enter only numeric values or commas in the product IDs field."
msgstr ""

#: parcel.module:66
msgid "Collection of Products"
msgstr ""

#: parcel.module:100
msgid "Products"
msgstr ""

#: parcel.module:104
msgid "Product IDs"
msgstr ""

#: parcel.module:108
msgid "Enter a comma separated list of product ids (nid) to add to this transaction. Here is a <a href=\"%product_quicklist\" onclick=\"window.open(this.href, '!product_quicklist', 'width=480,height=480,scrollbars=yes,status=yes'); return false\">list of all products</a>."
msgstr ""

#: parcel.module:156
msgid "Price"
msgstr ""

#: parcel.module:171
msgid "This product contains the following items"
msgstr ""

#: parcel.module:24
msgid "create collections of products"
msgstr ""

#: parcel.module:24
msgid "edit own collections of products"
msgstr ""

#: parcel.module:0
msgid "parcel"
msgstr ""

#: parcel.install:25
msgid "E-Commerce: Parcel product tables have been created."
msgstr ""

#: parcel.info:0
msgid "Parcel Product"
msgstr ""

#: parcel.info:0
msgid "Create packages of ecommerce items."
msgstr ""

#: parcel.info:0
msgid "E-Commerce Product Types"
msgstr ""

