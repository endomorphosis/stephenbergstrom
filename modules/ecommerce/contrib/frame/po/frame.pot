# $Id: frame.pot,v 1.1.2.1 2007/05/03 15:12:21 darrenoh Exp $
#
# LANGUAGE translation of Drupal (frame.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  frame.module,v 1.6.2.1.4.2 2007/02/04 03:07:42 gordon
#  frame.info,v 1.3.2.9 2007/02/25 13:54:57 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 10:52-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: frame.module:11
msgid "Creates an frame product."
msgstr ""

#: frame.module:48
msgid "Frame Product" 
msgstr ""

#: frame.module:19
msgid "create frame products"
msgstr ""

#: frame.module:19
msgid "edit own frame products"
msgstr ""

#: frame.module:0
msgid "frame"
msgstr ""

#: frame.info:0
msgid "Frame"
msgstr ""

#: frame.info:0
msgid "Frame products for E-Commerce."
msgstr ""

#: frame.info:0
msgid "E-Commerce Product Types"
msgstr ""

