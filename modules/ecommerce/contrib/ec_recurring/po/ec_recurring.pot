# $Id: ec_recurring.pot,v 1.1.2.1 2007/05/03 15:12:23 darrenoh Exp $
#
# LANGUAGE translation of Drupal (ec_recurring.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  ec_recurring.module,v 1.1.2.17.2.21 2007/04/09 13:12:59 gordon
#  ec_recurring.install,v 1.1.2.8.2.5 2007/02/16 02:52:22 sammys
#  ec_recurring.info,v 1.1.2.5 2007/02/17 12:36:40 gordon
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 10:59-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ec_recurring.module:117;124
msgid "Schedules"
msgstr ""

#: ec_recurring.module:132
msgid "Add expiry schedule"
msgstr ""

#: ec_recurring.module:141
msgid "Cron settings"
msgstr ""

#: ec_recurring.module:145
msgid "Configure your cron settings for processing schedules."
msgstr ""

#: ec_recurring.module:151
msgid "Renew item"
msgstr ""

#: ec_recurring.module:158;274
msgid "Recurring products"
msgstr ""

#: ec_recurring.module:160
msgid "List of all recurring products."
msgstr ""

#: ec_recurring.module:165
msgid "Active recurring products"
msgstr ""

#: ec_recurring.module:173
msgid "Order history"
msgstr ""

#: ec_recurring.module:184
msgid "Edit expiry schedule"
msgstr ""

#: ec_recurring.module:192;741
msgid "Add reminder"
msgstr ""

#: ec_recurring.module:201
msgid "Delete reminder"
msgstr ""

#: ec_recurring.module:209
msgid "Edit reminder"
msgstr ""

#: ec_recurring.module:227
msgid "Subscriptions"
msgstr ""

#: ec_recurring.module:228
msgid "View your subscriptions"
msgstr ""

#: ec_recurring.module:231
msgid "History"
msgstr ""

#: ec_recurring.module:283
msgid "Renewal schedule"
msgstr ""

#: ec_recurring.module:285
msgid "Select the renewal schedule to be used for this product. !link. NOTE: save your changes to this product first."
msgstr ""

#: ec_recurring.module:285
msgid "Add another schedule"
msgstr ""

#: ec_recurring.module:356
msgid "Processed %nprocessed expired products\n%nauto automated payments processed\n\n"
msgstr ""

#: ec_recurring.module:357
msgid "Processed %nprocessed expired reminders\nSent %nsent reminders\n%nfailed reminders failed\n\n"
msgstr ""

#: ec_recurring.module:386
msgid "Product expiry processed."
msgstr ""

#: ec_recurring.module:386
msgid "View transaction"
msgstr ""

#: ec_recurring.module:415;536;682
msgid "Edit"
msgstr ""

#: ec_recurring.module:416
msgid "Delete"
msgstr ""

#: ec_recurring.module:427
msgid "<b>WARNING:</b> Some unexpired products exist that use this schedule. Only name changes are possible until %date"
msgstr ""

#: ec_recurring.module:440
msgid "Name"
msgstr ""

#: ec_recurring.module:447
msgid "Number of units"
msgstr ""

#: ec_recurring.module:455;735
msgid "Unit"
msgstr ""

#: ec_recurring.module:463
msgid "Number of renewals"
msgstr ""

#: ec_recurring.module:465
msgid "Select the number of renewals this schedule has."
msgstr ""

#: ec_recurring.module:473
msgid "Example expiry date"
msgstr ""

#: ec_recurring.module:479
msgid "Reminder mails"
msgstr ""

#: ec_recurring.module:480
msgid "No reminders added to schedule. <a href=\"!url\">Add one</a>."
msgstr ""

#: ec_recurring.module:480;673;1492
msgid "name"
msgstr ""

#: ec_recurring.module:480;673
msgid "schedule"
msgstr ""

#: ec_recurring.module:480
msgid "example date"
msgstr ""

#: ec_recurring.module:480;673
msgid "op"
msgstr ""

#: ec_recurring.module:489
msgid "Update schedule"
msgstr ""

#: ec_recurring.module:489
msgid "Add schedule"
msgstr ""

#: ec_recurring.module:502
msgid "Name is too long"
msgstr ""

#: ec_recurring.module:509;756
msgid "Please select a unit"
msgstr ""

#: ec_recurring.module:518
msgid "Unit selected is too small for the reminder schedules to be valid"
msgstr ""

#: ec_recurring.module:534
msgid "Schedule added"
msgstr ""

#: ec_recurring.module:534
msgid "Schedule updated"
msgstr ""

#: ec_recurring.module:555
msgid "All unexpired events have been updated also"
msgstr ""

#: ec_recurring.module:675
msgid "No schedules were found"
msgstr ""

#: ec_recurring.module:714
msgid "Product expiry time"
msgstr ""

#: ec_recurring.module:722
msgid "Reminder mail to use"
msgstr ""

#: ec_recurring.module:725;1723
msgid "At product expiry"
msgstr ""

#: ec_recurring.module:728
msgid "Numunits"
msgstr ""

#: ec_recurring.module:731
msgid "Number of units before expiry of the product. Select <b>at expiry</b> if you want the mail sent when the product expires. You can select any unit for mails sent at expiry."
msgstr ""

#: ec_recurring.module:741
msgid "Update reminder"
msgstr ""

#: ec_recurring.module:749
msgid "Please select an numunits"
msgstr ""

#: ec_recurring.module:767
msgid "added"
msgstr ""

#: ec_recurring.module:767
msgid "updated"
msgstr ""

#: ec_recurring.module:768
msgid "Reminder email %op. "
msgstr ""

#: ec_recurring.module:770;1425
msgid "edit"
msgstr ""

#: ec_recurring.module:783
msgid "The new reminder has been added to all unexpired products."
msgstr ""

#: ec_recurring.module:790
msgid "The reminder has been updated for all unexpired products."
msgstr ""

#: ec_recurring.module:840
msgid "To change the expiry schedule of a product with unexpired purchases, you must create another version of the product."
msgstr ""

#: ec_recurring.module:1156
msgid "ACTIVE"
msgstr ""

#: ec_recurring.module:1158
msgid "EXPIRED"
msgstr ""

#: ec_recurring.module:1160
msgid "RENEWED"
msgstr ""

#: ec_recurring.module:1162
msgid "UNKNOWN"
msgstr ""

#: ec_recurring.module:1274
msgid "Reminder mail sent."
msgstr ""

#: ec_recurring.module:1274
msgid "view reminder"
msgstr ""

#: ec_recurring.module:1391
msgid "Only one entry of this product can be added to the cart"
msgstr ""

#: ec_recurring.module:1403
msgid "You have reached the limit of renewals for this product"
msgstr ""

#: ec_recurring.module:1417
msgid "title"
msgstr ""

#: ec_recurring.module:1424
msgid "view"
msgstr ""

#: ec_recurring.module:1432
msgid "Username"
msgstr ""

#: ec_recurring.module:1432
msgid "Product"
msgstr ""

#: ec_recurring.module:1432
msgid "Expiry"
msgstr ""

#: ec_recurring.module:1437
msgid "Anonymous"
msgstr ""

#: ec_recurring.module:1487
msgid "%username's subscriptions"
msgstr ""

#: ec_recurring.module:1492
msgid "price"
msgstr ""

#: ec_recurring.module:1492
msgid "expiry date"
msgstr ""

#: ec_recurring.module:1492
msgid "time to expiry"
msgstr ""

#: ec_recurring.module:1492
msgid "renewals remaining"
msgstr ""

#: ec_recurring.module:1492
msgid "operations"
msgstr ""

#: ec_recurring.module:1500
msgid "This item is in <a href=\"%cart_view\">your shopping cart</a>."
msgstr ""

#: ec_recurring.module:1504
msgid "renew"
msgstr ""

#: ec_recurring.module:1517
msgid "<p>You have no active subscriptions.</p>"
msgstr ""

#: ec_recurring.module:1525;1750
msgid "Unlimited"
msgstr ""

#: ec_recurring.module:1550
msgid "Hour of expiry"
msgstr ""

#: ec_recurring.module:1553
msgid "Select the hour of the day at which the cron system will expire products. NOTE: This only works if you have setup a cron task."
msgstr ""

#: ec_recurring.module:1686
msgid "before expiry"
msgstr ""

#: ec_recurring.module:1707
msgid "--"
msgstr ""

#: ec_recurring.module:1707
msgid "Day(s)"
msgstr ""

#: ec_recurring.module:1707
msgid "Week(s)"
msgstr ""

#: ec_recurring.module:1707
msgid "Month(s)"
msgstr ""

#: ec_recurring.module:1707
msgid "Year(s)"
msgstr ""

#: ec_recurring.module:1727
msgid "day"
msgstr ""

#: ec_recurring.module:1727
msgid "week"
msgstr ""

#: ec_recurring.module:1727
msgid "month"
msgstr ""

#: ec_recurring.module:1727
msgid "year"
msgstr ""

#: ec_recurring.module:1731
msgid "Unit undefined"
msgstr ""

#: ec_recurring.module:1751
msgid "No renewals"
msgstr ""

#: ec_recurring.module:1752
msgid "1 renewal"
msgstr ""

#: ec_recurring.module:1754
msgid "!i renewals"
msgstr ""

#: ec_recurring.module:1763
msgid "With unlimited renewals"
msgstr ""

#: ec_recurring.module:1766
msgid "With no option to renew"
msgstr ""

#: ec_recurring.module:1769
msgid "With %ncycles renewals"
msgstr ""

#: ec_recurring.module:2009
msgid "Product expiry date"
msgstr ""

#: ec_recurring.module:2010
msgid "Time to product expiry (in weeks and days)."
msgstr ""

#: ec_recurring.module:2047
msgid "Recurring product reminder"
msgstr ""

#: ec_recurring.module:2059
msgid "Default renewal reminder"
msgstr ""

#: ec_recurring.module:2060
msgid "Your %site renewal"
msgstr ""

#: ec_recurring.module:2061
msgid "Dear %billing_firstname %billing_lastname,\n\nis message is to inform you that the following item will expire in %time_to_expire.\n\n%renewal_item\n\nTo prevent this from happening, please renew the item as soon as possible.\n\nClick the link below to renew:\n%renewal_link\n\nThanks,\n\n%site\n%uri"
msgstr ""

#: ec_recurring.module:2063
msgid "Default expiration notice"
msgstr ""

#: ec_recurring.module:2064
msgid "Your %site renewal has expired"
msgstr ""

#: ec_recurring.module:2065
msgid "Dear %billing_firstname %billing_lastname,\n\nThis message is to inform you that the following item expired at %time_since_expired:\n\n%renewal_item\n\nThanks,\n\n%site\n%uri"
msgstr ""

#: ec_recurring.module:386
msgid "product_expiry"
msgstr ""

#: ec_recurring.module:536;770
msgid "ec_recurring"
msgstr ""

#: ec_recurring.module:1274
msgid "reminder_sent"
msgstr ""

#: ec_recurring.module:102
msgid "administer expiry schedules"
msgstr ""

#: ec_recurring.install:95
msgid "E-Commerce: Recurring tables have been created."
msgstr ""

#: ec_recurring.info:0
msgid "EC Recurring"
msgstr ""

#: ec_recurring.info:0
msgid "Enables recurring payments"
msgstr ""

#: ec_recurring.info:0
msgid "E-Commerce Uncategorized"
msgstr ""

