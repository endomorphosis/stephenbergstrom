<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <?php print $head ?>
  <title><?php print $head_title ?></title>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js" type="text/javascript"></script>
</head>

<body>
<?php //print_r($node); ?>
<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr>
    <td colspan="2" id="logo" style="position: absolute; top: 0; float: left;">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
    </td></tr><tr>
    <td colspan="2" id="menu">
<?php
$nodes = db_query('SELECT n.nid, n.title, r.teaser, s.price, s.ptype, s.sku FROM {node} n INNER JOIN {node_revisions} r ON n.vid = r.vid INNER JOIN {ec_product} s ON s.vid = n.vid WHERE n.type = \'image\' AND n.sticky = 1  ORDER BY n.changed DESC, n.created DESC LIMIT 0, 30');

while($row = db_fetch_array($nodes))
  {
$node_var[] = $row;
  }
//print_r($node_var);
//$nid = node_load($node_var['nid']);
//print_r($node_var);
print '<MARQUEE><table><tr>';
for ($a = 0; $node_var[$a]; $a++)
{
$nid = node_load($node_var[$a]['nid']);
//print($nid->images['thumbnail']);
   
    print '<td><a href="?q=node/'.$nid->nid.'">';
    if ($nid->images['preview']){ 
    $mysock = getimagesize($nid->images['preview']); 
    print "<img src=\"./";
    print $nid->images['preview'];
    print '"';
    print ' width="'.( $mysock[0] / 3.5) .'" height="' . ($mysock[1] /3.5). '" ';
    print "/></a>"."</td>";
    }
    else{

    print '<img src="./files/no-image.png"/>';
    }
}
print '</tr></table></MARQUEE>';


?>
    </td>
  </tr>
  <tr>
    <td colspan="2"><div><?php print $header ?></div></td>
  </tr>
<tr>
    <td id="menu">
      <center><?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?><?php } ?>
      <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
</center>
    </td>
</tr>

</table>

<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
 
	  <? if($is_front){ ?>
          <center><div id="main" style="min-height: 600px;" >   <a href="" id="thumb-url"><img src="" id="slideshow" style="display: none; margin: auto; padding: auto; max-width: 800px; max-height: 600px;"><img src="" width="0" height="0" id="slideshow2" style=" margin: auto; padding: auto; max-width: 800px; max-height: 600px;">
	   <br/><span id="title"> </span></a>
	    <script type="text/javascript">
		$.fn.pause = function(duration) {
		    $(this).animate({ dummy: 1 }, duration);
		    return this;
		};
		
		function runIt(){
                $.post("image.php", {}, function(data){
		var img = data.image;
		var title = data.title;
		var teaser = data.teaser;
		var nid = data.nid;
		$("#title").text(title);
		$("#slideshow").hide();
		$("#thumb-url").attr('href', "?q=node/" + nid);
		$("#slideshow2").attr("src", img);
		$("#slideshow").attr("src", $("#slideshow2").attr("src")).hide();
		$("#title").fadeIn(1000);
		$("#slideshow").fadeIn(1000);
		$("#title").pause(5000).fadeOut(1000);
		$("#slideshow").pause(5000).fadeOut(1000, runIt);
		}, "json");  
		}

		runIt();
	    </script>
          <? } else {?>
        <div id="main" height="600" >
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
	  <? } ?>
      </div>
    </td>

    <?php if ($sidebar_right) { ?><td id="sidebar-right" width="100">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>

<div id="footer">
  <?php print $footer_message ?>
</div>
<?php print $closure ?>
</body>
</html>
