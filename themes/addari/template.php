<?php

function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}


function addari_regions() {
  return array(
	   'content_top' => t('content top'),
	   'content_bottom' => t('content bottom'),
	   'sidebar_left' => t('left sidebar'),
       'sidebar_right' => t('right sidebar'),
	   'header' => t('header'),
	   'footer' => t('footer'),


  );
}


?>